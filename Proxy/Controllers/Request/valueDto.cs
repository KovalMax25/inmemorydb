﻿namespace Proxy.Controllers.Request
{
	public class ValueDto
	{
		public string Value { get; set; }
	}
}
