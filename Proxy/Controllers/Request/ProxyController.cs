﻿using Microsoft.AspNetCore.Mvc;
using Proxy.Helpers.HttpClientHelpers;
using Proxy.Helpers.Routing;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Proxy.Controllers.Request
{
	[ApiController]
	[Route("proxy")]
	public class ProxyController : ControllerBase
	{
		private Configuration _config;
		private HttpClient _client;
		private HttpClientHelper _httpClientHelper;

		Dictionary<int, string> nodes = new Dictionary<int, string>();

		public ProxyController(Configuration config, HttpClient client, HttpClientHelper httpClientHelper)
		{
			_httpClientHelper = httpClientHelper;
			_client = client;
			_config = config;
		}

		[HttpGet("{id}")]
		public async Task<string> GetAsync(int id)
		{
			foreach (var data in _config.NodeDataList)
			{
				nodes.Add(data.nodeNumber, data.nodeURL);
			}

			var shardNum = RoutingHelper.GetShardNumber(id, _config);

			var nodeUrl = nodes[shardNum];

			return await _httpClientHelper.GetValue(nodeUrl, id);
		}

		[HttpPut("{id}")]
		public async Task<string> PutAsync([FromBody] ValueDto dto, int id)
		{
			foreach (var nodeData in _config.NodeDataList)
			{
				nodes.Add(nodeData.nodeNumber, nodeData.nodeURL);
			}

			var shardNum = RoutingHelper.GetShardNumber(id, _config);

			var nodeURL = nodes[shardNum];
			return await _httpClientHelper.SendValueToNode(nodeURL, id, dto.Value);
		}

		[HttpDelete("{id}")]
		public async Task<string> DeleteAsync(int id)
		{
			foreach (var nodeData in _config.NodeDataList)
			{
				nodes.Add(nodeData.nodeNumber, nodeData.nodeURL);
			}

			var shardNum = RoutingHelper.GetShardNumber(id, _config);

			var nodeURL = nodes[shardNum];
			return await _httpClientHelper.DeleteValue(nodeURL, id);
		}
	}
}
