﻿using Microsoft.AspNetCore.Mvc;
using Proxy.Controllers.Config.Dtos;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Proxy.Helpers.Routing;
using Proxy.Helpers.HttpClientHelpers;
using Proxy.Helpers.Resharding;

namespace Proxy.Controllers.Config
{
	[ApiController]
	public class ConfigController : ControllerBase
	{
		private Configuration _config;
		private HttpClientHelper _httpClientHelper;
		private ReshardingHelper _reshardingHelper;

		public static List<int> valuesForResharding = new List<int>();

		public ConfigController(Configuration config, HttpClientHelper httpClientHelper, ReshardingHelper reshardingHelper)
		{
			_httpClientHelper = httpClientHelper;
			_reshardingHelper = reshardingHelper;
			_config = config;
		}

		[HttpPost]
		[Route("config/startNodes")]
		public void StartNodes([FromBody] NewNodesConfigDto dto)
		{
			foreach (var data in dto.NodesConfig)
			{
				var nodeDataConfig = new ConfigNodeData
				{
					nodeNumber = data.NodeNumber,
					nodePort = data.NodePort,
					nodeURL = data.NodeURL,
					isMaster = data.IsMaster,
					slaveUrls = data.SlaveUrls
				};

				_config.NodeCount += 1;
				_config.NodeDataList.Add(nodeDataConfig);
			}

			foreach (var port in _config.NodeDataList.Select(x => x.nodePort))
			{
				string command = @"cd C:\Users\Developer\source\repos\In-memoryDb\In-memoryDb & script.bat " + port;
				Process.Start("cmd.exe", "/C " + command);
			}
		}

		[HttpGet]
		[Route("config/setConfig")]
		public async Task SetNodesConfigAsync()
		{
			foreach (var node in _config.NodeDataList)
			{
				var nodeUrls = _config.NodeDataList.Where(x => x.nodeURL != node.nodeURL).Select(y => y.nodeURL).ToList();
				await _httpClientHelper.SendNodesConfigAsync(node.nodeURL, node.nodeNumber, nodeUrls, node.slaveUrls);
			}

			await _reshardingHelper.ReshardingAsync(_config, _httpClientHelper);
		}

		[HttpGet]
		[Route("config/send")]
		public async Task Send()
		{
			for (int i = 0; i < 5000; i++)
			{
				var shardNum = RoutingHelper.GetShardNumber(i, _config);
				var node = _config.NodeDataList[shardNum];

				await _httpClientHelper.SendReshardingValuesAsync(node.nodeURL, i, i.ToString());
			}
		}
	}
}
