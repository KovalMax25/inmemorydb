﻿namespace Proxy.Controllers.Config

{
	public class ReshardingDataDto
	{
		public int Number { get; set; }
		public int Amount { get; set; }
		public int MyNum { get; set; }
		public string Value { get; set; }
	}
}
