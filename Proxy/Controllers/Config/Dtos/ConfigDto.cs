﻿using System.Collections.Generic;

namespace Proxy.Controllers.Config.Dtos
{
	public class ConfigDto
	{
		public int NodeNumber;
		public string NodeURL;
		public int NodePort;
		public bool IsMaster;
		public List<string> SlaveUrls;
	}
}
