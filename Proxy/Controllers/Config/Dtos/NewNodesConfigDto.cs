﻿using System.Collections.Generic;

namespace Proxy.Controllers.Config.Dtos
{
	public class NewNodesConfigDto
	{
		public List<ConfigDto> NodesConfig { get; set; }
	}
}
