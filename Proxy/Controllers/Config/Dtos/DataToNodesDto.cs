﻿using System;
using System.Collections.Generic;

namespace Proxy.Controllers.Config.Dtos
{
	public class DataToNodesDto
	{
		public int NodeNumber { get; set; }
		public List<string> NodeUrls { get; set; }
		public int NodeCount { get; set; }
		public bool IsMaster { get; set; }
		public List<string> SlaveUrls { get; set; }
	}
}