﻿using System.Collections.Generic;

namespace Proxy.Controllers.Config
{
	public class ConfigNodeData
	{
		public int nodeNumber;
		public string nodeURL;
		public int nodePort;
		public bool isMaster;
		public List<string> slaveUrls;
	}
}
