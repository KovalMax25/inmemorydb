﻿using Proxy.Helpers.HttpClientHelpers;
using System;
using System.Linq;
using System.Threading.Tasks;
using Proxy.Helpers.Routing;

namespace Proxy.Helpers.Resharding
{
	public class ReshardingHelper
	{
		public async Task ReshardingAsync(Configuration _config, HttpClientHelper _httpClientHelper)
		{
			foreach (var node in _config.NodeDataList)
			{
				var nodesUrls = _config.NodeDataList.Where(x => x.nodeURL != node.nodeURL).Select(y => y.nodeURL).ToList();
				var result = await _httpClientHelper.SendDataForReshardingAsync(node.nodeURL, node.nodeNumber, nodesUrls);

				if (result.Count != 0)
				{
					_config.ReshardingIds.AddRange(result);
					foreach (var str in _config.ReshardingIds)
					{
						var a = str.Split(';');
						_config.ReshardingData.TryAdd(Convert.ToInt32(a[0]), a[1]);
					}
				}
			}

			foreach (var value in _config.ReshardingData)
			{
				foreach (var node in _config.NodeDataList)
				{
					if (RoutingHelper.GetShardNumber(value.Key, _config) == node.nodeNumber)
					{
						await _httpClientHelper.SendReshardingValuesAsync(node.nodeURL, value.Key, value.Value);
					}
				}
			}
		}
	}
}
