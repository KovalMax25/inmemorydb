﻿namespace Proxy.Helpers.Routing
{
	public class RoutingHelper
	{
		public static int GetShardNumber(int id, Configuration config)
		{
			var shardNum = id.GetHashCode() % config.NodeDataList.Count;

			return shardNum;
		}
	}
}
