﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Net.Http;
using Proxy.Controllers.Config.Dtos;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using System.Text;
using Proxy.Controllers.Config;
using Proxy.Controllers.Request;
using System.Net;

namespace Proxy.Helpers.HttpClientHelpers
{
	public class HttpClientHelper
	{
		private static HttpClient _client;

		public HttpClientHelper(HttpClient client)
		{
			_client = client;
		}

		public async Task SendNodesConfigAsync(string nodeUrl, int nodeNumber, List<string> nodeUrls, List<string> slaveUrls)
		{
			string resourceAddress = nodeUrl + "node/setConfig";
			if (slaveUrls == null)
			{
				slaveUrls = new List<string>();
			}
			DataToNodesDto data = new DataToNodesDto { NodeNumber = nodeNumber, NodeUrls = nodeUrls, SlaveUrls = slaveUrls};
			string postBody = JsonConvert.SerializeObject(data);
			_client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
			HttpResponseMessage wcfResponse = await _client.PostAsync(resourceAddress, new StringContent(postBody, Encoding.UTF8, "application/json"));
		}

		public async Task<List<string>> SendDataForReshardingAsync(string nodeUrl, int nodeNumber, List<string> nodesUrls)
		{
			string resourceAddress = nodeUrl + "api/database/getReshardingValues";
			ReshardingDataDto data = new ReshardingDataDto { Number = nodeNumber, Amount = nodesUrls.Count + 1 };
			string postBody = JsonConvert.SerializeObject(data);
			_client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
			HttpResponseMessage wcfResponse = await _client.PostAsync(resourceAddress, new StringContent(postBody, Encoding.UTF8, "application/json"));
			var result = wcfResponse.Content.ReadAsAsync<List<string>>().GetAwaiter().GetResult();
			return result;
		}

		public async Task SendReshardingValuesAsync(string nodeUrl, int id, string value)
		{
			string resourceAddress = nodeUrl + "api/database/" + id;
			ReshardingDataDto resData = new ReshardingDataDto() { Value = value };
			string postBody = JsonConvert.SerializeObject(resData);
			var response = await _client.PutAsync(resourceAddress, new StringContent(postBody, Encoding.UTF8, "application/json"));
			string content = await response.Content.ReadAsStringAsync();
		}

		public async Task<string> SendValueToNode(string nodeUrl, int id, string value)
		{
			string resourceAddress = nodeUrl + "api/database/" + id;
			string responseContent;

			ValueDto resData = new ValueDto() { Value = value };
			string postBody = JsonConvert.SerializeObject(resData);
			var response = await _client.PutAsync(resourceAddress, new StringContent(postBody, Encoding.UTF8, "application/json"));
			string content = await response.Content.ReadAsStringAsync();			

			if (response.StatusCode == HttpStatusCode.OK)
			{
				responseContent = JsonConvert.DeserializeObject(content).ToString();
			}
			else responseContent = value;

			return responseContent;
		}

		public async Task<string> GetValue(string nodeUrl, int id)
		{
			string resourceAddress = nodeUrl + "api/database/" + id;
			var response = await _client.GetAsync(resourceAddress);
			string content = await response.Content.ReadAsStringAsync();
			var responseContent = JsonConvert.DeserializeObject(content).ToString();

			return responseContent;
		}

		public async Task<string> DeleteValue(string nodeUrl, int id)
		{
			string resourceAddress = nodeUrl + "api/database/" + id;
			var response = await _client.DeleteAsync(resourceAddress);
			string content = await response.Content.ReadAsStringAsync();
			var responseContent = JsonConvert.DeserializeObject(content).ToString();

			return responseContent;
		}
	}
}
