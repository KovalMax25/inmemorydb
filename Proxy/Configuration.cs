﻿using Proxy.Controllers.Config;
using System.Collections.Generic;

namespace Proxy
{
	public class Configuration
	{
		public int NodeCount { get; set; }
		public List<ConfigNodeData> NodeDataList = new List<ConfigNodeData>();
		public List<string> ReshardingIds = new List<string>();
		public Dictionary<int, string> ReshardingData = new Dictionary<int, string>();
	}
}
