﻿using InMemoryDb.Data;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InMemoryDb.Domain
{
	public class Configuration
	{
		private static int nodeNumber;
		public int NodeCount { get; set; }
		private static List<string> slaveUrls = new List<string>();

		private Database _db;
		private ReplicationData _replicationData;

		public Configuration(Database db, ReplicationData replicationData)
		{
			_db = db;
			_replicationData = replicationData;
		}

		public void SetNodeNumber(int number)
		{
			nodeNumber = number;
		}
		
		public void SetSlaveUrls(List<string> urls)
		{
			slaveUrls.AddRange(urls);
		}

		public int GetNodeNumber()
		{
			return nodeNumber;
		}

		public List<string> GetSlaveUrls()
		{
			return slaveUrls;
		}

		public async Task RestoreDatabaseAsync(int nodeNumber)
		{
			await _db.Restore(nodeNumber);
			await _replicationData.Restore(nodeNumber);
		}
	}
}
