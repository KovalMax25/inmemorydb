﻿using System.Threading.Tasks;
using System.Net.Http;
using System.Net;
using System.Text;
using Newtonsoft.Json;
using InMemoryDb.Replication.Dtos;

namespace InMemoryDb.Helpers.HttpClientHelper
{
	public class RepHttpClient
	{
		private static HttpClient _client;

		public RepHttpClient()
		{
			_client = new HttpClient();
		}

		public async Task SendAndWaitAsync(string replicaUrl, int id, string value, int nodeNumber)
		{
			HttpStatusCode statusCode = 0;

			string resourceAddress = replicaUrl + "api/replication/" + id;
			ReplicationValueDto resData = new ReplicationValueDto() { Value = value, nodeNumber = nodeNumber };
			string postBody = JsonConvert.SerializeObject(resData);

			while (statusCode != HttpStatusCode.OK)
			{
				var response = await _client.PutAsync(resourceAddress, new StringContent(postBody, Encoding.UTF8, "application/json"));
				statusCode = response.StatusCode;
			}
		}

		public async Task SendAndNotWaitAsync(string replicaUrl, int id, string value, int nodeNumber)
		{
			string resourceAddress = replicaUrl + "api/replication/" + id;
			ReplicationValueDto resData = new ReplicationValueDto() { Value = value, nodeNumber = nodeNumber };
			string postBody = JsonConvert.SerializeObject(resData);
			var response = await _client.PutAsync(resourceAddress, new StringContent(postBody, Encoding.UTF8, "application/json")).ConfigureAwait(false);
		}

		public async Task DeleteAndNotWaitAsync(string replicaUrl, int id)
		{
			string resourceAddress = replicaUrl + "api/replication/" + id;			
			var response = await _client.DeleteAsync(resourceAddress).ConfigureAwait(false);
		}

		public async Task DeleteAndWaitAsync(string replicaUrl, int id)
		{
			string resourceAddress = replicaUrl + "api/replication/" + id;
			HttpStatusCode statusCode = 0;

			while (statusCode != HttpStatusCode.OK)
			{
				var response = await _client.DeleteAsync(resourceAddress);
				statusCode = response.StatusCode;
			}
		}
	}
}
