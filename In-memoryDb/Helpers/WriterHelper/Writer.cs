﻿using System.Collections.Concurrent;
using System.IO;
using System.Threading.Tasks;

namespace InMemoryDb.Helpers.WriterHelper
{
	public class Writer
	{
		public async Task Write(string path, int nodeNum, ConcurrentDictionary<int, string> data)
		{
			var writer = new StreamWriter(path, true);

			foreach (var str in data)
			{
				await writer.WriteLineAsync(str.Key.ToString() + ',' + str.Value);
			}
			
			writer.Close();
		}
	}
}
