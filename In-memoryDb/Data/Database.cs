﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace InMemoryDb.Data
{
	public class Database
	{
		public static ConcurrentDictionary<int, string> dataBase = new ConcurrentDictionary<int, string>();
		public static ConcurrentDictionary<int, long> indexTable = new ConcurrentDictionary<int, long>();		

		public ConcurrentDictionary<int, string> GetDataBase()
		{
			return dataBase;
		}

		public ConcurrentDictionary<int, long> GetIndexTable()
		{
			return indexTable;
		}

		public string GetAsync(int key)
		{
			dataBase.TryGetValue(key, out string value);

			return value;
		}		

		public List<string> Get()
        {
            return dataBase.Values.ToList();
        }

		public string AddOrUpdate(int key, string value)
        {
            dataBase.AddOrUpdate
				(key,value,
				(index, oldValue) => value);

			dataBase.TryGetValue(key, out string data);

			return data;
		}

        public string Delete(int key)
        {
            dataBase.TryRemove(key, out string data);
            return data;
        }

		public List<string> GetReshardingValues(int number, int amount)
		{
			var ids = dataBase.Keys.Where(x => x.GetHashCode() % amount != number);
			List<string> ReshardingValues = new List<string>();

			foreach (var id in ids)
			{
				ReshardingValues.Add(id.ToString() + ";" + GetAsync(id).Result.ToString());
				Delete(id);
			}

			return ReshardingValues;
		}

		public async Task Restore(int nodeNumber)
		{
			var data = await File.ReadAllLinesAsync("node" + nodeNumber + "/data.txt");
			var dataBase = GetDataBase();

			foreach (var dataString in data)
			{
				var str = dataString.Split(',');
				dataBase.TryAdd(Convert.ToInt32(str[0]), str[1]);
			}
		}
	}
}
