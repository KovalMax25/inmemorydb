﻿using InMemoryDb.Domain;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace InMemoryDb.Data
{
	public class ReplicationData
	{
		public static ConcurrentDictionary<int, string> replicationDatabase = new ConcurrentDictionary<int, string>();

		public Configuration _configuration;

		public ConcurrentDictionary<int, string> GetReplicationDataBase()
		{
			return replicationDatabase;
		}

		public string AddOrUpdateReplicaData(int key, string value)
		{
			replicationDatabase.AddOrUpdate
				(key, value,
				(index, oldValue) => value);

			replicationDatabase.TryGetValue(key, out string data);

			return data;
		}

		public string DeleteReplicaData(int key)
		{
			replicationDatabase.TryRemove(key, out string data);
			return data;
		}

		public async Task<string> GetReplicaDataAsync(int key)
		{
			if (replicationDatabase.TryGetValue(key, out string value))
			{
				return await Task.FromResult(value);
			}

			return value;
		}

		public List<string> GetReplicaData()
		{
			return replicationDatabase.Values.ToList();
		}

		public async Task Restore(int nodeNumber)
		{
			var data = await File.ReadAllLinesAsync("node" + nodeNumber + "/replica/replica.txt");

			foreach (var dataString in data)
			{
				var str = dataString.Split(',');
				replicationDatabase.TryAdd(Convert.ToInt32(str[0]), str[1]);
			}

			var ids = replicationDatabase.Keys.Where(x => x.GetHashCode() % _configuration.NodeCount != _configuration.GetNodeNumber());			

			foreach (var id in ids)
			{
				DeleteReplicaData(id);
			}
		}
	}
}
