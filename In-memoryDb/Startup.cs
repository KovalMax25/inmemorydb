﻿using InMemoryDb.Data;
using InMemoryDb.Domain;
using InMemoryDb.Helpers.HttpClientHelper;
using InMemoryDb.Helpers.WriterHelper;
using InMemoryDb.Replication;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace In_memoryDb
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options => options.AddPolicy("AllowAll", p => p.AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader()));

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddSingleton(typeof(Database));
			services.AddSingleton(typeof(ReplicationData));
			services.AddSingleton(typeof(Configuration));
			services.AddTransient(typeof(Writer));
			services.AddSingleton(typeof(RepHttpClient));

			var replicationModel = Configuration.GetSection("ReplicationModel").Value;

			if (replicationModel == "Sync")
			{
				services.AddTransient<IReplication, SyncReplication>();
			}
			else services.AddTransient<IReplication, AsyncReplication>();
		}

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IApplicationLifetime applicationLifetime)
        {
			//app.UsePathBase("/api/database");
            app.UseDefaultFiles();
            app.UseStaticFiles();

            app.UseCors(builder => builder.AllowAnyHeader()
                .AllowAnyOrigin()
                .AllowAnyMethod());

            if (env.IsDevelopment())
                app.UseDeveloperExceptionPage();
            else
                app.UseHsts();

            app.UseAuthentication();
            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
