﻿using InMemoryDb.Data;
using InMemoryDb.Domain;
using InMemoryDb.Helpers.HttpClientHelper;
using System.Threading.Tasks;

namespace InMemoryDb.Replication
{
	public class SyncReplication : IReplication
	{
		private static Database _db;
		private static Configuration _config;
		private static RepHttpClient _repHttpClient;

		public SyncReplication(Database db, Configuration config, RepHttpClient repHttpClient)
		{
			_db = db;
			_config = config;
			_repHttpClient = repHttpClient;
		}

		public async Task PutReplicationData(int id, string value, int nodeNumber)
		{
			foreach (var replicaUrl in _config.GetSlaveUrls())
			{
				await _repHttpClient.SendAndWaitAsync(replicaUrl, id, value, nodeNumber);				
			}
		}

		public async Task DeleteReplicationData(int id)
		{
			foreach (var replicaUrl in _config.GetSlaveUrls())
			{
				await _repHttpClient.DeleteAndWaitAsync(replicaUrl, id);
			}
		}
	}
}
