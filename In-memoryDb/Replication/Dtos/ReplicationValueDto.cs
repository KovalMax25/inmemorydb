﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InMemoryDb.Replication.Dtos
{
	public class ReplicationValueDto
	{
		public string Value;
		public int nodeNumber;
	}
}
