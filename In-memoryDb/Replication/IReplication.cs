﻿using System.Threading.Tasks;

namespace InMemoryDb.Replication
{
	public interface IReplication
	{
		Task PutReplicationData(int id, string value, int nodeNumber);
		Task DeleteReplicationData(int id);
	}
}
