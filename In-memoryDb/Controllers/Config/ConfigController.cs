﻿using InMemoryDb.Controllers.Config.Dtos;
using InMemoryDb.Data;
using InMemoryDb.Domain;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace InMemoryDb.Controllers.Config
{
	[ApiController]
	public class ConfigController : ControllerBase
	{
		private Configuration _config;

		public ConfigController(Configuration config)
		{
			_config = config;
		}

		[HttpPost]
		[Route("node/setConfig")]
		public async Task SetNodeConfigAsync([FromBody] ConfigDto dto)
		{
			_config.SetNodeNumber(dto.nodeNumber);
			_config.NodeCount = dto.NodeCount;
			_config.SetSlaveUrls(dto.SlaveUrls);

			await _config.RestoreDatabaseAsync(_config.GetNodeNumber());
		}
	}
}