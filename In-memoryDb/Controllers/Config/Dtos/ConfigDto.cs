﻿using System.Collections.Generic;

namespace InMemoryDb.Controllers.Config.Dtos
{
	public class ConfigDto
	{
		public int nodeNumber;
		public int NodeCount { get; set; }
		public List<string> SlaveUrls { get; set; }
	}
}
