﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace InMemoryDb.Controllers.InMemoryDatabase.Dtos
{
	public class ReshardingDataDto
	{
		public int number { get; set; }
		public int amount { get; set; }
		public int myNum { get; set; }
	}
}
