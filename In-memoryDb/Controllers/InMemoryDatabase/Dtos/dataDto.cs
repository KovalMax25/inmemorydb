﻿namespace InMemoryDb.Controllers.InMemoryDatabase.Dtos
{
    public class DataDto
    {
        public DataDto(string value)
        {
            Value = value;
        }

        public string Value { get; set; }
		public int nodeNumber;
    }
}
