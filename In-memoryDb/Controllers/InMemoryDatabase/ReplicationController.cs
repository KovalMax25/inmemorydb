﻿using InMemoryDb.Controllers.InMemoryDatabase.Dtos;
using InMemoryDb.Data;
using InMemoryDb.Domain;
using InMemoryDb.Helpers.WriterHelper;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InMemoryDb.Controllers.InMemoryDatabase
{
	[ApiController]
	[Route("api/[controller]")]
	public class ReplicationController : ControllerBase
	{
		private ReplicationData _replication;
		private Configuration _config;
		private Writer _writer;

		public ReplicationController(ReplicationData replication, Configuration config, Writer writer)
		{
			_replication = replication;
			_config = config;
			_writer = writer;
		}

		[HttpGet]
		public List<string> Get()
		{
			return _replication.GetReplicaData();
		}

		[HttpGet("{id}")]
		public async Task<string> GetAsync(int id)
		{
			return await _replication.GetReplicaDataAsync(id);
		}

		[HttpPut("{id}")]
		public async Task CreateReplicationData([FromBody] DataDto dto, int id)
		{
			_replication.AddOrUpdateReplicaData(id, dto.Value);

			var dataFileName = "node" + _config.GetNodeNumber() + "/replica/replica.txt";
			await _writer.Write(dataFileName, _config.GetNodeNumber(), _replication.GetReplicationDataBase());
		}

		[HttpDelete("{id}")]
		public string DeleteReplicationData(int id)
		{
			return _replication.DeleteReplicaData(id);
		}
	}
}
