﻿using System.Collections.Generic;
using System.Threading.Tasks;
using InMemoryDb.Controllers.InMemoryDatabase.Dtos;
using InMemoryDb.Data;
using InMemoryDb.Domain;
using InMemoryDb.Helpers.WriterHelper;
using InMemoryDb.Replication;
using Microsoft.AspNetCore.Mvc;

namespace InMemoryDb.Controllers.InMemoryDatabase
{
	[ApiController]
	[Route("api/[controller]")]
	public class DatabaseController : ControllerBase
	{
		private Database _dataBase;
		private Configuration _config;
		private IReplication _replication;
		private Writer _writer;

		public DatabaseController(Database db, Configuration config, IReplication replication, Writer writer)
		{
			_dataBase = db;
			_config = config;
			_replication = replication;
			_writer = writer;
		}

		[HttpGet]
		public List<string> Get()
		{
			return _dataBase.Get();
		}

		[HttpGet]
		[Route("count")]
		public int GetCount()
		{
			return _dataBase.Get().Count;
		}

		[HttpGet("{id}")]
		public string GetAsync(int id)
        {
			return _dataBase.GetAsync(id);
		}

        [HttpPut("{id}")]
		public async Task<string> Create([FromBody] DataDto dto, int id)
        {
			var writedData = _dataBase.AddOrUpdate(id, dto.Value);

			if (_config.GetSlaveUrls().Count != 0)
			{
				await _replication.PutReplicationData(id, dto.Value, _config.GetNodeNumber());				
			}

			var dataFileName = "node" + _config.GetNodeNumber() + "/data.txt";
			await _writer.Write(dataFileName, _config.GetNodeNumber(), _dataBase.GetDataBase());

			return writedData;
		}

		[HttpDelete("{id}")]
		public async Task<string> DeleteAsync(int id)
        {			
			var deletedData = _dataBase.Delete(id);

			if (_config.GetSlaveUrls().Count != 0)
			{
				await _replication.DeleteReplicationData(id);				
			}

			var dataFileName = "node" + _config.GetNodeNumber() + "/data.txt";
			await _writer.Write(dataFileName, _config.GetNodeNumber(), _dataBase.GetDataBase());

			return deletedData;
		}

		[HttpPost]
		[Route("getReshardingValues")]
		public List<string> GetReshardingValues([FromBody] ReshardingDataDto dto)
		{
			return _dataBase.GetReshardingValues(dto.number, dto.amount);
		}
	}
}
