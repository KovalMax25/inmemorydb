﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace DataBaseTests
{
    public class ApiTests
    {
		HttpClient httpClient = new HttpClient();

		Dictionary<int, string> sendedData;
		Dictionary<int, string> gettedData;

		string resourceAddress = "http://localhost:10000/api/database/";		

		private async Task TestPutMethodAsync()
		{
			Request request = new Request();
			sendedData = await request.Put(resourceAddress, "String number - ");
			gettedData = await request.Get(resourceAddress);
			Assert.Equal(sendedData, gettedData);
		}

		private async Task TestUpdateDataAsync()
		{
			Request request = new Request();
			sendedData = await request.Put(resourceAddress, "Не Запись не № - ");
			gettedData = await request.Get(resourceAddress);
			Assert.Equal(sendedData, gettedData);
		}

		private async Task TestDeleteMethodAsync()
		{
			for (int i = 0; i < 200; i++)
			{
				var response = await httpClient.DeleteAsync(resourceAddress + i);
			}

			for (int i = 0; i < 200; i++)
			{
				var response = await httpClient.GetAsync(resourceAddress + i);
				Assert.Equal(HttpStatusCode.NoContent, response.StatusCode);
			}
		}
	

        [Fact]
        public async Task TestPutAsync()
        {
			var tasks = new List<Task>
			{
				TestPutMethodAsync(),
				TestPutMethodAsync()
			};

			await Task.WhenAll(tasks);
		}

		[Fact]
		public async Task TestUpdateAsync()
		{
			var tasks = new List<Task>
			{
				TestUpdateDataAsync(),
				TestUpdateDataAsync()
			};

			await Task.WhenAll(tasks);
		}

		[Fact]
		public async Task TestDeleteAsync()
		{
			var tasks = new List<Task>
			{
				TestDeleteMethodAsync(),
				TestDeleteMethodAsync()
			};

			await Task.WhenAll(tasks);
		}
	}
}
