﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace DataBaseTests
{
	public class ProxyTests
	{
		
		List<int> counts = new List<int>();

		Dictionary<int, string> sendedData;
		Dictionary<int, string> gettedData;

		readonly string proxyUrl = "http://localhost:7000/proxy/";
		readonly string nodeOne = "http://localhost:5000/api/database/count";
		readonly string nodeTwo = "http://localhost:6000/api/database/count";
		readonly string nodeThree = "http://localhost:8000/api/database/count";
		readonly string nodeFour = "http://localhost:9000/api/database/count";

		private async Task TestTwoNodesAsync()
		{
			Request request = new Request();
			sendedData = await request.Put(proxyUrl, "String number - ");
			gettedData = await request.Get(proxyUrl);
			Assert.Equal(sendedData, gettedData);

			List<string> urls = new List<string>()
			{
				nodeOne,
				nodeTwo
			};

			counts = await request.GetCount(urls);

			Assert.True(counts.All(i => i == counts.First()));
		}

		private async Task TestFourNodesAsync()
		{
			Request request = new Request();

			List<string> urls = new List<string>()
			{
				nodeOne,
				nodeTwo,
				nodeThree,
				nodeFour
			};
			counts = await request.GetCount(urls);

			Assert.True(counts.All(i => i == counts.First()));
		}

		[Fact]
		public async Task Test1Async()
		{
			var tasks = new List<Task>
			{
				TestTwoNodesAsync(),
				TestTwoNodesAsync()
			};

			await Task.WhenAll(tasks);
		}

		[Fact]
		public async Task Test2Async()
		{
			var tasks = new List<Task>
			{
				TestFourNodesAsync(),
				TestFourNodesAsync()
			};

			await Task.WhenAll(tasks);
		}
	}
}
