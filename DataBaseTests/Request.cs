﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace DataBaseTests
{
	public class Request
	{
		HttpClient httpClient = new HttpClient();
		private readonly string resourceAddress;

		/*public Request(string url)
		{
			resourceAddress = url;
		}*/

		public async Task<Dictionary<int, string>> Put(string url, string value)
		{
			Dictionary<int, string> valuesList = new Dictionary<int, string>();

			for (int i = 0; i < 200; i++)
			{
				valuesList.TryAdd(i, value + i);
				Data data = new Data { Value = value + i };
				string postBody = JsonConvert.SerializeObject(data);
				httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
				await httpClient.PutAsync(url + i, new StringContent(postBody, Encoding.UTF8, "application/json"));
			}

			return valuesList;
		}

		public async Task<Dictionary<int, string>> Get(string url)
		{
			Dictionary<int, string> responseValuesList = new Dictionary<int, string>();

			for (int i = 0; i < 200; i++)
			{
				var response = await httpClient.GetAsync(url + i);
				string content = await response.Content.ReadAsStringAsync();
				string a = JsonConvert.DeserializeObject(content).ToString();
				responseValuesList.TryAdd(i, a);
			}

			return responseValuesList;
		}

		public async Task<List<int>> GetCount(List<string> urls)
		{
			List<int> counts = new List<int>();

			foreach (var url in urls)
			{
				var countResponse = await httpClient.GetAsync(url);
				string count = await countResponse.Content.ReadAsStringAsync();
				string a1 = JsonConvert.DeserializeObject(count).ToString();

				counts.Add(Convert.ToInt32(a1));
			}

			return counts;
		}
	}
}
