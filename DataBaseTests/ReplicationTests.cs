﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;

namespace DataBaseTests
{
	public class ReplicationTests
	{
		Dictionary<int, string> sendedData;
		Dictionary<int, string> gettedData;

		string masterUrl = "http://localhost:5000/api/database/";
		string firstSlaveUrl = "http://localhost:6000/api/replication/";
		string secondSlaveUrl = "http://localhost:8000/api/replication/";

		private async Task MasterSlaveTest()
		{
			Request request = new Request();

			sendedData = await request.Put(masterUrl, "String number - ");
			gettedData = await request.Get(firstSlaveUrl);

			Assert.Equal(sendedData, gettedData);

			gettedData = await request.Get(secondSlaveUrl);

			Assert.Equal(sendedData, gettedData);
		}

		[Fact]
		public async Task Test1Async()
		{
			var tasks = new List<Task>
			{
				MasterSlaveTest(),
				MasterSlaveTest()
			};

			await Task.WhenAll(tasks);
		}
	}
}
